#!/bin/env perl

use strict;
use warnings;

use File::Basename;
use FindBin '$Bin';
use lib '$Bin';
use SampleDefinitions;
use PipelineConf;
use Utils qw/system_call get_option/;
use Term::ANSIColor;

$Utils::log_file = "install.log";

my %conf_all = %PipelineConf::conf;
my %conf = %{$conf_all{"default"}};

my %progs = (
    diamond_bin => sub {
        my $dir = dirname(get_conf("diamond_bin"));
        if (!check_mkdir($dir)) {
            system_call("wget http://github.com/bbuchfink/diamond/releases/download/v0.8.36/diamond-linux64.tar.gz -O $dir/diamond-linux64.tar.gz");
            system_call("tar xvvf $dir/diamond-linux64.tar.gz -C $dir");
        }
    },
    diamond_nr_database => sub {
        my $dir = dirname(get_conf("diamond_nr_database"));
        if (!check_mkdir($dir)) {
            system_call("curl ftp://ftp.ncbi.nlm.nih.gov/blast/db/FASTA/nr.gz | gunzip -c > $dir/nr.faa");
            system_call(get_conf("diamond_bin")." makedb --in $dir/nr.faa -d $dir/nr");
        }
    },
    kraken_bin => sub {
        my $dir = get_conf("kraken_dir");
        if (!check_mkdir($dir)) {
            system_call("git clone https://github.com/fbreitwieser/kraken.git $dir");
            system_call("cd $dir && ./install_kraken.sh `pwd`/install");
        }
    },
    kraken_databases => sub {
        my $dir = get_conf("kraken_filter_database");
        if ("
        perl ~/projects/metagenomics-scripts/db-scripts/create_new_database.pl --db h+p+v-170207 --preset h+p+v --proc 20

    },
    test => sub { 
        system_call("while [ 1 == 1 ]; do echo i && sleep 3; done");
    }
);

my $usage = colored(basename($0)." [OPTIONS] TARGETs\n","bold").
"TARGETS: 
\t".join(", ", sort keys %progs)."
OPTIONS:";

$usage = Utils::get_options($usage);

if (scalar(@ARGV) == 0) {
    print $usage;
    exit 0;
}


foreach my $prog (@ARGV) {
    if (!defined $progs{$prog}) {
        print "Target $prog does not exist !! \n";
        print $usage;
        exit 1;
    }
}

foreach my $prog (@ARGV) {
    print "Executing target $prog ... \n";
    &{$progs{$prog}}();
}



###############

sub check_mkdir {
    my ($dir) = @_;
    if (-d $dir) {
        print STDERR "$dir exists.\n";
        return 1;
    } else {
        print STDERR "$dir does not exist - creating it.\n";
        system_call("mkdir -p $dir");
        return 0;
    }
}

sub get_conf {
    my ($c) = @_;
    die "$c is not specified in the configuration (PipelineConf.pm)" if (!defined $conf{$c});
    return $conf{$c};
}


