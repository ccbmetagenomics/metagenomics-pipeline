#!/bin/env perl

use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use FindBin '$Bin';
use lib "$Bin/../lib";
use Utils qw/system_call system_call_f system_call_l get_option option_defined/;
use PipelineConf;
use Term::ANSIColor;

my %conf_all = %PipelineConf::conf;
my %conf = %{$conf_all{"default"}};
my $checkmark = colored("✓","green");

my $scripts_dir = "/home/fbreitwieser/projects/metagenomics-scripts/db-scripts";

my $usage = basename($0);
$usage = 
    Utils::get_options($usage,
        ['db-dir=s',   "Database base directory.", "db" ],
        ['timestamp=s', "Timestamp - important!!", undef],
        ['all-targets', "Build all targets", 0],
        ['all-databases', "Build all databases", 0],
        ['proc=i', "Number of threads", 10],
        ['force', "Force (remove directories first!)", 0]
    );

my $nproc = get_option('proc');

die "Define db-dir\n$usage" unless (option_defined('db-dir'));
die "Define timestamp\n$usage" unless (option_defined('timestamp'));

my %databases = (
    "kraken/filter_k25" => [
        ['human','mmus','contaminants'],
        "{kraken-build} --db {db-dir} --build --jellyfish-hash-size 10000M --kmer-len 25 --minimizer-len 12"
    ],
    "kraken/archaea_k25" => [
        ['archaea'],
        "{kraken-build} --db {db-dir} --build --jellyfish-hash-size 10000M --kmer-len 25 --minimizer-len 12"
    ],
    "kraken/p+h+v" => [
        ['human','contaminants', 'bacteria', 'archaea', 'all-viral', 'viral-strains'],
        "{kraken-build} --db {db-dir} --build --jellyfish-hash-size 40000M"
    ],
    "centrifuge/p+h+v" => [
        ['human','contaminants', 'bacteria', 'archaea', 'all-viral', 'viral-strains'],
        "{centrifuge-build} --db {db-dir} --build --jellyfish-hash-size 40000M"
    ],

#    "p+v" => [[],"{kraken-build} --db {db-dir} --build --jellyfish-hash-size 40000M"]
);

my $seq_dir = "seqs";

sub make_seqid_map {
    my ($dir, $taxid) = @_;
    system_call_f("grep '^>' $dir/$seq_dir/*.fn?a | sed -e 's/^>//' -e 's/ .*//' -e 's/\$/\\t$taxid/'","$dir/seqid2taxid.map");
}

# That was useful for fixing the column 20/21 error in viral genomes assembly_summary.txt:
# cut -f 6,20,21 assembly_summary.txt | sed 's/^\(.*\)\t\(ftp:.*\)\t.*/\1\t\2/;s/^\(.*\)\t.*\t\(ftp:.*\)/\1\t\2/' | while read taxid next; do BN=`basename $next`_genomic_dustmasked.fna; if [[ ! -z $BN ]]; then echo $BN; curl ${next}/`basename $next`_genomic.fna.gz | gunzip -c | sed "s/^>/>kraken:taxid|$taxid /" | dustmasker -infmt fasta -in /dev/stdin -outfmt fasta | sed '/^>/! s/[^AGCT]/N/g' > $BN; fi; done

my %targets = (
    "human" => sub {
        my ($dir) = @_;
        system_call_f("curl ftp://ftp.ncbi.nlm.nih.gov/blast/db/FASTA/human_genomic.gz | gunzip -c","$dir/$seq_dir/human_genomic.fna");
        system_call_f("curl ftp://ftp.ncbi.nlm.nih.gov/genomes/Homo_sapiens/RNA/rna.fa.gz | gunzip -c","$dir/$seq_dir/human_rna.fna");
        make_seqid_map("$dir",9606);
    },
    "mmus"     => sub { system_call("$scripts_dir/download-mmus-reference-genome $_[0]"); },
    "contaminants" => sub { system_call("$scripts_dir/get-contaminant-seq $_[0]/$seq_dir"); },
    "bacteria" => sub { system_call_f("$scripts_dir/centrifuge-download -o $_[0]/$seq_dir -P $nproc -d bacteria -a 'Complete Genome' -m refseq","$_[0]/seqid2taxid.map"); },
    "archaea" => sub { system_call_f("$scripts_dir/centrifuge-download -o $_[0]/$seq_dir -P $nproc -d archaea -a 'Complete Genome' -m refseq","$_[0]/seqid2taxid.map"); },
    "all-viral" => sub { system_call_f("$scripts_dir/centrifuge-download -o $_[0]/$seq_dir -P $nproc -d viral -a 'Any' -m refseq","$_[0]/seqid2taxid.map"); },
    "viral-strains" => sub {
        my ($db_dir,$tax_dir) = @_;
        die "TaxDir??" unless -d $tax_dir;
        system_call_f("$scripts_dir/download-viral-genomes.pl $db_dir viral $nproc $tax_dir/names.dmp", "$db_dir/seqid2taxid.map");
    },
    "viroid-strains" => sub {
        my ($db_dir,$tax_dir) = @_;
        die "TaxDir??" unless -d $tax_dir;
        system_call_f("$scripts_dir/download-viral-genomes.pl $db_dir viroid $nproc $tax_dir/names.dmp", "$db_dir/seqid2taxid.map");
    }

);

my $timestamp = get_option("timestamp");
my $db_dir = get_option("db-dir");
my $common_dir = "$db_dir/common-$timestamp";
my $log_file = basename($0).".log";
reset_log_file();

my $tax_dir = "$common_dir/taxonomy";
if (!-f "$common_dir/taxonomy/taxonomy.complete") {
    system_call("mkdir -p $common_dir/taxonomy");
    system_call("curl ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz -o $tax_dir/taxdump.tar.gz && tar xvvf $tax_dir/taxdump.tar.gz -C $tax_dir");
    system_call("touch $common_dir/taxonomy/taxonomy.complete");
}

if (get_option("all-targets")) {
    foreach my $target (sort keys %targets) {
        build_target($target);
    }
} else {
    my @databases = get_option("all-databases")? sort keys %databases : @ARGV;

for my $db_target (@databases) {
    die "$db_target not defined as database" unless defined $databases{$db_target};
    my $db = $databases{$db_target};
    foreach my $target (@{$db->[0]}) {
        die "$target not defined as target" unless defined $targets{$target};
    }
}

for my $db_target (@databases) {
    print STDERR "Looking at $db_target ...\n";
    my $db_target_dir = get_option("db-dir")."/$db_target-$timestamp";
    if (get_option("force") && -d $db_target_dir) {
        system_call("rm -rf $db_target_dir");
    }
    
    if (-f "$db_target_dir/all.complete") {
        print STDERR " - $db_target_dir/all.complete present $checkmark\n";
        next;
    }

    system_call("rm -rf $db_target_dir && mkdir -p $db_target_dir");
    ## download targets to common directory
    my $db = $databases{$db_target};
    foreach my $target (@{$db->[0]}) {
        build_target($target);

        ## link targets to the database dir (if required)
    }
    
    $Utils::log_file = "$db_target_dir/build.log";

    ## build database
    my $cmd = $db->[1];
    $cmd =~ s/{db-dir}/$db_target_dir/;

    print STDERR `pwd`;
    system_call("touch $db_target_dir/all.complete");
    reset_log_file();
}
}

sub build_target {
    my ($target) = @_;
    print STDERR " --> target $target ";
    my $dir = "$common_dir/$target";
    if (get_option("force") && -d $dir) {
        system_call("rm -rf $dir");
    }

    if (! -z "$dir/$target.fna") {
        print STDERR "building ...\n";
        if (!-f "$dir/seqid2taxid.map" || !-d "$dir/seqs") {
            system_call("rm -rf $dir && mkdir -p $dir/seqs");
            &{$targets{$target}}("$common_dir/$target",$tax_dir);
        }
        system_call_f("find $dir/seqs -iname '*.fn\\?a' -o -iname '*.fasta' -exec cat {} +","$dir/$target.fna");
    }
    print STDERR $checkmark,"\n";

}

sub reset_log_file {
    $Utils::log_file = $log_file;
}


