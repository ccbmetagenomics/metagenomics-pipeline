#! /usr/bin/env perl
#
# run-pipeline.pl picks up files in the staging directory,
#  and processes them according to information encoded in their
#  file name
#
# Author fbreitwieser <fbreitwieser@igm3>
# Version 1.0
# Copyright (C) 2017 fbreitwieser <fbreitwieser@igm3>
# Modified On 2017-02-05 16:06
# Created  2015-04-21 16:06
#
use strict;
use warnings;

use File::Basename;
use File::Copy;
use File::stat;
use Term::ANSIColor;
use Getopt::Long;

use FindBin '$Bin';
use lib '$Bin';
use SampleDefinitions;
use PipelineConf;
use Utils qw/get_option option_defined system_call/;
use Text::Wrap;

my %did_warn;
my $system_col = "white on_black";

my %sample_type = %SampleDefinitions::sample_type;
foreach my $s (keys %sample_type) {
    if (!defined ($sample_type{$s}->{"staging_dir"})) {
        $sample_type{$s}->{"staging_dir"} = "fastq-files/$s";
    }
}
my @sample_types = sort keys %sample_type;

my $host_name = `hostname`; chomp $host_name;
my %conf_all = %PipelineConf::conf;
die "Configuration for host '$host_name' is not defined!" unless defined $conf_all{$host_name};
my %conf = %{$conf_all{$host_name}};

-d "locks" or `mkdir -p locks`;
my $checkmark = colored("✓","green");
my $fastq_to_fasta = "sed -n '1~4s/^@/>/p;2~4p'";
my $processing_dir = "processing";

my %file_sizes;
my %analyzed_files;

$Text::Wrap::columns = 80;

my $usage = colored(basename($0)." [OPTIONS] SAMPLE_TYPE\n","bold").
"SAMPLE_TYPEs (use 'list' for details): 
".wrap("\t","\t",join(", ", @sample_types))."
OPTIONS:";

$usage = Utils::get_options($usage,
    #['force',        "regenerate files",   0 ],
    ['threads=i',    "number of threads",  10 ],
    #['sample-type=s', "sample type", "" ],
    #['staging-dir=s',"staging directory",  'staging' ],
    ['pattern=s',"Only take samples following a pattern",undef],
    #['filter!',      "filter human, mouse, and rRNA with bowtie2 beforehand?", 0 ],
    ['q-filter', "quality-filter extracted reads", 0],
    [],
#    ['PROGRAMS'],
    ['filter-w-kraken=s', "Filter all reads matching to specified Kraken database", undef],
    [],
    ['blastx!',"run blastx", 0],
    ['use-kraken-db=s',"Use a different kraken DB then the default one (specified in conf). The result files will get the basename of the DB appended.", undef],
    ['centrifuge',"run centrifuge", 0],
    ['centrifuge2',"run centrifuge2", 0],
    ['humann2',"run humann2", 0],
    ['hisat2',"run hisat2", 0],
    ['kallisto',"run kallisto", 0],
    ['bracken',"run Bracken", 0],
    [],
    ['align-to=s',  "align samples to genome idx",undef],
    ['extract-reads=s', "extract reads for specific tax ids", undef],
    ['extract-reads-name=s', "Name for extract reads for specific tax ids", undef],
    ['extract-unidentified-reads', "Get the unidentified fraction (requires a previous Kraken run)", 0],
    ['extract-nonhuman-reads', "extract reads that are neither human nor synthetic constructs", 0],
    #['use-argv', "use FASTQ provided as arguments", 0],
    #['extended-kraken-report',"call kraken-report-modif", 0],
    ['against-unidentified-reads', "Run the search against the unidentified fraction (requires a previous Kraken run)", 0],
    ## Hidden options
    ['check-files!', "check if files are being modified", 0, 0 ],
    #['force-kraken',"force running kraken",0],
    #['force-kraken-report',"force running kraken-report",0],
    #['interactive',"interactive mode", 0],
    #['reverse',"reverse processing input files",0]
);

#my $usage = colored("run-pipeline.pl [options]","bold");

my $threads = get_option("threads");
my $bam_params = "--threads $threads";

sub get_sample_type_files {
    my ($my_sample_type) = @_;
    my @files;
	opendir(my $STAGING_DIR,$my_sample_type->{"staging_dir"}) or return @files;
    foreach my $fastq_file (sort readdir($STAGING_DIR)) {
        next if ($fastq_file =~ m/^\./);
	    next unless ($fastq_file =~ /$my_sample_type->{ext}$/);

        #next if ($my_sample_type->{paired} && !($fastq_file =~ /_1/));
        my $basename = basename($fastq_file,$my_sample_type->{'ext'});
        my $pattern = $my_sample_type->{fname_pattern};
        $pattern =~ s/%/1/ if $my_sample_type->{paired};
        my @re_res = ($basename =~ /^$pattern$/);

        if (option_defined("pattern")) {
            my $fname_pattern = get_option("pattern");
            next unless $fastq_file =~ /$fname_pattern/;
        }

        next unless (scalar @re_res >= 1);
        push @files, $fastq_file;
    }
    close ($STAGING_DIR);
    return @files;
}

foreach my $sample_type_name (@ARGV) {
if ($sample_type_name eq "list") {
    print "Available sample types: \n";
    foreach my $s (sort keys %sample_type) {
        my $dir = $sample_type{$s}->{"staging_dir"};
        print colored($s,"blue")," in ",
            colored($dir, (-d $dir ? "green" : "red"));
        print " [",(scalar get_sample_type_files($sample_type{$s})), " matching files]","\n";
    }
    exit;
}
    print("Give a valid sample type (",join(", ",@sample_types),") .\n".$usage), exit unless defined $sample_type{$sample_type_name};

}

if (scalar(@ARGV) == 0) {
    print $usage;
    exit;
}
 
foreach my $sample_type_name (@ARGV) {
my $my_sample_type=$sample_type{$sample_type_name};
my $staging_dir = $my_sample_type->{'staging_dir'};

print STDERR colored("
ANALYZING $sample_type_name
    staging dir: $staging_dir/
","blue");

my $cnt = 0;
    #while (1) {
	    opendir(my $STAGING_DIR,$staging_dir) or die $1;
        my @files = sort readdir($STAGING_DIR);
        #@files = reverse(@files) if get_option("reverse");
	    foreach my $fastq_file (@files) {
	        next if ($fastq_file =~ m/^\./);
	        next unless ($fastq_file =~ /$my_sample_type->{ext}$/);

            #next if ($my_sample_type->{paired} && !($fastq_file =~ /_1/));
            my $basename = basename($fastq_file,$my_sample_type->{'ext'});
            #next unless ($basename =~ /^$my_sample_type->{fname_pattern}$/);

            $fastq_file = "$staging_dir/$fastq_file";
            next if $analyzed_files{$fastq_file};
	
	        if (get_option("check-files")) {
		        ## check if file is open
                #if (system("list-my-open-files | grep `pwd`/$fastq_file") == 0) {
		            #file found in the list - skipping it
                    #    print "list-my-open-files | grep `pwd`/$fastq_file was succesful - skipping the file for now\n";
                    #} 
		
		        ## check file sizes
		        my $file_size = (stat($fastq_file))[7];
		        if (!defined $file_sizes{$fastq_file}) {
		            $file_sizes{$fastq_file} = $file_size;
		            print "+"; $|++; next;
                }
                
                if ($file_sizes{$fastq_file} ne $file_size) {
		            $file_sizes{$fastq_file} = $file_size;
		            print "~"; $|++; next;
		        }
	        }
	
            #print_blue("Processing file $fastq_file\n");
	        check_and_process_file($sample_type_name, $fastq_file,1);
            $analyzed_files{$fastq_file} = 1;
	    }
        #    sleep 2;
        #if ($cnt++ eq 90) {
        #    $cnt=0;
        #    print ".";
        #}
        closedir($STAGING_DIR);
        #} 


}

############################ SUBS

sub print_red { print colored("@_","red"); }
sub print_green { print colored("@_","green"); }
sub print_blue { print colored("@_","blue"); }

sub check_and_process_file {
    my ($sample_type_name, $fastq_file,$move_to_processing) = @_;
    my $my_sample_type = $sample_type{$sample_type_name};
    my $basename = basename($fastq_file,$my_sample_type->{'ext'});

    ## make sure file has the correct things encoded in its name and extract it
    my $pattern = $my_sample_type->{fname_pattern};
    $pattern =~ s/%/1/ if $my_sample_type->{paired};
    my @re_res = ($basename =~ /^$pattern$/);

    if (option_defined("pattern")) {
        my $fname_pattern = get_option("pattern");
        return unless $fastq_file =~ /$fname_pattern/;
    }

    if (scalar @re_res < 1) {
        #print STDERR "! $basename =~ $pattern: @re_res\n";
        return;
    }   
    my $sample_code = $re_res[0];

    die "sample_code"  unless defined $sample_code;
    my $is_paired = $my_sample_type->{paired};

    if ($is_paired) {
        (my $fq2 = $fastq_file) =~ s/%/2/;
        -f $fq2 or die "Mate pair 2 file $fq2 is not present";
    }

    my $seq_type = $my_sample_type->{sequencing_type};
    if ($seq_type ne 'RNA' && $seq_type ne 'DNA') {
        $seq_type = $re_res[$seq_type];
    }
    die "seq_type = $seq_type? " unless ($seq_type eq 'RNA' or $seq_type eq 'DNA');

    ### TODO: This way of recovering the pairing information is prone to break - fix based on pattern!
    #$basename =~ s/(.*)_1/$1_%/ if $is_paired;
    #$fastq_file =~ s/(.*)_1/$1_%/ if $is_paired;
    
    if ($is_paired) {
        $basename =~ s/_R1/_R%/;
        $basename =~ s/_1/_%/;
        $fastq_file =~ s/_R1/_R%/;
        $fastq_file =~ s/_1/_%/; ## TODO: Fix it - this is dangerous
    }

    #print "BASENAME: $basename\n";
    ## ok to go!
    my $is_rnaseq = $seq_type eq "RNA";
    #next if ($is_rnaseq);
    my $lock_file = "locks/$basename.lock";
    if (-f $lock_file) {
        print STDERR "Skipping $sample_code, lock file $lock_file exists\n";
    } else {
        `date > $lock_file`;
        do_process($sample_type_name,$fastq_file,$basename,$sample_code,$is_rnaseq,$is_paired);
        `rm -v $lock_file`;
    }
}

sub warn_once {
    my ($fastq_file, $msg) = @_;
    if (!defined $did_warn{$fastq_file}) {
        print colored "$msg\n", "red";
        $did_warn{$fastq_file} = 1;
    }
    return 0;
}

sub do_process {
    my ($sample_type_name, $fastq_file,$basename,$patient_code,$is_rnaseq,$is_paired,$move_fasta) = @_;

    print ("Checking $sample_type_name/".colored($patient_code,"blue").": "); $|++;

    ## create directory structure
    my $my_processing_dir = "$processing_dir/$sample_type_name/$patient_code";
    -d $my_processing_dir || `mkdir -vp $my_processing_dir`;
    -d "$my_processing_dir/fastq" || `mkdir -vp $my_processing_dir/fastq`;

    my $new_fastq_file = "$my_processing_dir/fastq/$basename.fastq.gz";

    # print STDERR "AAAAAAA::::  $new_fastq_file\n";
    if (defined $move_fasta && $move_fasta) {
        move($fastq_file,$new_fastq_file);
    } else {
        $new_fastq_file = $fastq_file;
    }

    ## postfix for the bowtie output files
    my $postfix = ""; #$is_paired? "_%" : "";
    my @file_ps = $is_paired? ("1","2") : (undef);

    if (get_option("filter")) {
        -d "$my_processing_dir/align" || `mkdir -vp $my_processing_dir/align`;
	    ## TODO: Should I use hisat / tophat2 to remove spliced reads, or is bowtie --local sensitive enough?
	    ## filter human reads from FASTQ file
	    $new_fastq_file = filter_w_bowtie(
	        $new_fastq_file,
	        "$my_processing_dir/fastq/$basename$postfix.filtered1_no-hs.fastq",
	        "$my_processing_dir/align/$basename.hs.bam",
	        get_conf("hs_bowtie2_idx"),
	        $is_paired
	    );
	
	    ## filter mouse reads from FASTQ file
	    $new_fastq_file = filter_w_bowtie(
	        $new_fastq_file,
	        "$my_processing_dir/fastq/$basename$postfix.filtered2_no-hs_mm.fastq",
	        "$my_processing_dir/align/$basename.mm.bam",
	        get_conf("mm_bowtie2_idx"),
	        $is_paired);

    }

    if (get_option("hisat2")) {
        -d "$my_processing_dir/align" || `mkdir -vp $my_processing_dir/align`;
	    filter_w_hisat(
	        $new_fastq_file,
	        "$my_processing_dir/fastq/$basename$postfix.filtered1_no-hs.fastq",
	        "$my_processing_dir/align/$basename.hisat2.bam",
	        get_conf("hs_hisat2_idx"),
	        $is_paired
	    );
    }

	    ## filter rRNA and SILVA vector sequences
#	    if ($is_rnaseq) {
#	        $new_fastq_file = run_sortmerna(
#	            $new_fastq_file,
#	            "$my_processing_dir/fastq/$basename$postfix.no_rrna",
#	            "$my_processing_dir/fastq/$basename$postfix.rrna",
#	            $rrna_bowtie2_idx,
#	            $is_paired);
#	    } 
    my @files = ($new_fastq_file);
    if ($is_paired) {

        # print STDERR "AAAAAAA::::  $new_fastq_file\n";
        die "No '%' is there for the paired data" if (!$new_fastq_file =~ /%/);
        
        (my $input_fastq1 = $new_fastq_file) =~ s/%/1/ or die "$new_fastq_file file pattern??";
        (my $input_fastq2 = $new_fastq_file) =~ s/%/2/ or die "$new_fastq_file file pattern??";
        if (!-s $input_fastq2) {
            die "Mate pair 2 is not available - expecting \n\t$input_fastq1 and\n\t$input_fastq2\n. Pattern is $new_fastq_file.\n";
        }
        @files = ($input_fastq1,$input_fastq2);
    }

    ###########################################################################
    ## run KRAKEN

    my $basename_m = $basename;
    $basename_m =~ s/_%// if $is_paired;
    $basename_m =~ s/_R%// if $is_paired;

    my $kraken_db = get_option_or_default("use-kraken-db", get_conf("kraken_db"));
    my $name_suffix .= ".".basename($kraken_db);

    my $kraken0_fastq = "$my_processing_dir/fastq/$basename.unidentified.fastq.gz";
    if (get_option("against-unidentified")) {
        $name_suffix .= ".against0";
        ## TODO: Works only for paired end, currently
        @files = (get_p($kraken0_fastq,1), get_p($kraken0_fastq,2));
        if (!-s get_p($kraken0_fastq,2)) {
            print STDERR "$files[0] does not exist!\n";
            next;
        }
    }

    my $kraken_file = "$my_processing_dir/kraken/$basename_m$name_suffix.kraken";
    my $krakengz_file = "$my_processing_dir/kraken/$basename_m$name_suffix.kraken.gz";
    my $kraken_report = "$my_processing_dir/report/$basename_m$name_suffix.report";
    if (get_option('extended-kraken-report')) {
        $kraken_report = "$my_processing_dir/report/$basename_m$name_suffix.report_extended";
    }
    -d "$my_processing_dir/kraken" || `mkdir -vp $my_processing_dir/kraken`;
    if (check_file_ok($kraken_file,undef,'force-kraken')) {
        system_call("kraken --db $kraken_db -t $threads".
                    ($is_paired? " --paired" : "").
                    ($files[0] =~ /.gz/? " --gzip-compressed" : "").
                    " --fastq-input @files".
                    " | kraken-host /dev/stdin > $kraken_file");
    } else {
        print " kraken$checkmark"; $|++;
    }
    -d "$my_processing_dir/report" || `mkdir -vp $my_processing_dir/report`;
    if (check_file_ok($kraken_report,$kraken_file,'force-kraken-report')) {
        print STDERR "\n";
        #if (stat($kraken_file)->mtime > stat($kraken_report)->mtime) {
        ## create a kraken report
        if (!get_option('extended-kraken-report')) {

            system_call(get_conf("kraken_report_script"),
                    " --db=$kraken_db $kraken_file > $kraken_report");
            #system_call("$kraken_report_xlsx $kraken_report");
            } else {
            system_call("kraken-report-modif ".
                    " --db=$kraken_db $kraken_file".
                    ($is_paired? " --paired" : "").
                    " $new_fastq_file > $kraken_report");
            next;
            }
    } else {
        print " report$checkmark"; $|++;
    }
    

    ###########################################################################
    ## extract reads w/ specified taxid
    my @extract_reads_files;
    if (option_defined("extract-reads")) {
        for my $taxid (split(/,/,get_option("extract-reads"))) {
        print STDERR "extract-reads\n";
        #my ($res) = system_exec("grep \"[^\t]*\t[^\t]*\t[^\t]*\t[^\t]*\t$taxid\t\" $kraken_report");
        my ($res) = `grep \"\t$taxid\t\" $kraken_report`;
        next unless defined $res;
        chomp $res;
        my @res = split(/\t/,$res);
        my $name;
        if (defined $res[$#res]) {
            $name = $res[$#res];
            $name =~ s/^ *//g;
            $name =~ s/[^A-Za-z0-9\-\.]/_/g;

        } else {
            print STDERR "No res\n";
        }
            #print STDERR "$res[9]\n";
           	        my $extract_reads_file = "$my_processing_dir/fastq/$basename.$name.$taxid.fq";
            push @extract_reads_files, $extract_reads_file; 

            if (check_file_ok(get_p($extract_reads_file,2),undef,undef)) {
            
            foreach my $file_p (@file_ps) {
                system_exec(
	                "print-all-reads-matching-taxa.perl".
	                " ".$taxid.
	                " ".$kraken_file.
                    " ".get_p($new_fastq_file,$file_p)," > ".get_p($extract_reads_file,$file_p));
	        }
            }
        }
    }

    ###########################################################################
    ## extract non-human reads
  
    #my $kraken0_fastq = "$my_processing_dir/fastq/$basename.unidentified.fastq";
    my $unidentified_reads_fa = "$my_processing_dir/fastq/$basename.unidentified.fasta";
    my $unidentified_reads_fa_np = "$my_processing_dir/fastq/$basename_m.unidentified.fasta";
    if (option_defined("get-unidentified")) {
    if (!-s get_p($kraken0_fastq,2) || get_option("force")) {
        ## extract unidentified reads
        my $cmd = "print-all-reads-matching-taxa.perl 0 $kraken_file";
        foreach my $file_p (@file_ps) {
            system_exec($cmd." ".get_p($new_fastq_file,$file_p)," | gzip -c > ".get_p($kraken0_fastq,$file_p));
        }
    }
    if (option_defined("make-fasta")) {
    if (!-s get_p($unidentified_reads_fa,2) || get_option("force")) {
      foreach my $file_p (@file_ps) {
        system_call("$fastq_to_fasta ".get_p($kraken0_fastq,$file_p).
            "> ".get_p($unidentified_reads_fa,$file_p));
      }
    }
    }
}

    if (get_option("extract-nonhuman-reads")) {
    }

    my $filter_taxid = "9606,32630,81077";
    my $nohuman_reads_fq = "$my_processing_dir/fastq/$basename.nohuman.fastq.gz";
    my $nohuman_reads_fq_filtered = "$my_processing_dir/fastq/$basename.nohuman.q_filtered.fastq.gz";
    my $nohuman_reads_fa = "$my_processing_dir/fastq/$basename.nohuman.q_filtered.fasta";
    my $nohuman_reads_fa_np = "$my_processing_dir/fastq/$basename_m.nohuman.q_filtered.fasta";
 
    if (!-s get_p($nohuman_reads_fq,2) || get_option("force")) {
        ## extract nohuman reads
        my $cmd = "print-all-reads-matching-taxa.perl -i $filter_taxid $kraken_file";
        foreach my $file_p (@file_ps) {
            system_exec($cmd." ".get_p($new_fastq_file,$file_p)," | gzip -c > ".get_p($nohuman_reads_fq,$file_p));
        }
    }

    if (option_defined("q-filter")) {
    if (!-s get_p($nohuman_reads_fq_filtered,2) || get_option("force")) {
        ## extract nohuman reads
        foreach my $file_p (@file_ps) {
            my $cmd = sprintf("gunzip -c %s | /scratch0/igm3/fbreitwieser/test-microbiomics-sw/bbmap/bbduk.sh in=stdin.fq out=stdout.fq qtrim=rl trimq=5 entropy=0.9 | gzip -c > %s 2> %s.log",
                              get_p($nohuman_reads_fq,$file_p),get_p($nohuman_reads_fq_filtered,$file_p),get_p($nohuman_reads_fq_filtered,$file_p));
            system_exec($cmd);
        }
    }
}



    if (option_defined("make-fasta")) {
    if (!-s get_p($nohuman_reads_fa,2) || get_option("force")) {
      foreach my $file_p (@file_ps) {
        system_call("$fastq_to_fasta ".get_p($nohuman_reads_fq_filtered,$file_p).
            " > ".get_p($nohuman_reads_fa,$file_p));
      }
    }
}

    #my $filter_microbial_taxid = "9606,32630,81077,0,1";
    my $microbial_names = "d_Bacteria k_Fungi d_Viruses";
    my $microbial_reads_fa = "$my_processing_dir/fastq/$basename.microbial.fasta";
    my $microbial_reads_fq = "$my_processing_dir/fastq/$basename.microbial.fastq";
    my $microbial_reads_fa_np = "$my_processing_dir/fastq/$basename_m.microbial.fasta";
 
    if (0) {
    my $microbial_taxid = `Rscript get-taxids.R $kraken_report $microbial_names`; chomp $microbial_taxid;
    if (!-s get_p($microbial_reads_fq,2) || get_option("force")) {
        ## extract microbial reads
        my $cmd = "print-all-reads-matching-taxa.perl $microbial_taxid $kraken_file";
        foreach my $file_p (@file_ps) {
            system_exec($cmd." ".get_p($new_fastq_file,$file_p)," > ".get_p($microbial_reads_fq,$file_p));
        }
    }

    if (!-s get_p($microbial_reads_fa,2) || get_option("force")) {
      foreach my $file_p (@file_ps) {
        system_call("$fastq_to_fasta ".get_p($microbial_reads_fq,$file_p).
            " > ".get_p($microbial_reads_fa,$file_p));
      }
    }
    }

    ###########################################################################
    ## align to a specified genome
    if (option_defined("align-to")) {
        my $ref_genome = get_option("align-to");
        if (!-f $ref_genome.".1.bt2") {
            print STDERR "Should I create a bowtie2 index using the files ".$ref_genome."/*.fna? [Yn]    ";
            my $answer = <STDIN>; chomp $answer;
            if ($answer eq "" || $answer =~ /^[yY]/) {
                my $reference_in = `ls $ref_genome/*.{fna,fa,fasta} | paste -sd,`;
                chomp $reference_in;
                system_call("bowtie2-build $reference_in ".$ref_genome);
            } else {
                next;
            }
        }
        #my $search_file = $kraken0_fastq;
        #my $search_file = $kraken0_fastq;
        my $search_file = $nohuman_reads_fq;
        #my $search_file = $nohuman_reads_fq_filtered;
        #my $search_file = $new_fastq_file;
        if (!option_defined("extract-reads")) {
          print STDERR "Use option extract-reads when aligning to a genome to specify reads." ;
        } else {
          $search_file = join(",",@extract_reads_files);
        }
        -d "$my_processing_dir/align" || `mkdir -vp $my_processing_dir/align`;
        my $fname="$my_processing_dir/align/$basename_m.".basename(dirname($ref_genome)).".bam";
        my $fname_rmdup="$my_processing_dir/align/$basename_m.".basename(dirname($ref_genome))."-rmdup.bam";
	    filter_w_bowtie(
                $search_file,
	            undef,
                $fname,
	            $ref_genome,
	            $is_paired, "-L 31");
        system_call("samtools rmdup $fname $fname_rmdup");
    }

    ###########################################################################
    ## run METAPHLAN
    -d "$my_processing_dir/metaphlan" || `mkdir -vp $my_processing_dir/metaphlan`;
    my $metaphlan_file = "$my_processing_dir/metaphlan/$basename_m.metaphlan.txt";
    my $metaphlan_bt = "$my_processing_dir/metaphlan/$basename_m.metaphlan.bowtie.bz2";
    if (!-s $metaphlan_file || get_option("force")) {
        my $metaphlan_input_file = $nohuman_reads_fq;
        if ($is_paired) {
            $metaphlan_input_file = get_p($nohuman_reads_fq,1).",".get_p($nohuman_reads_fq,2);
        }
        system_call("time",get_conf("metaphlan_bin"),"$metaphlan_input_file --nproc $threads --mpa_pkl ".get_conf("mpa_dir")."/db_v20/mpa_v20_m200.pkl".
                    " --bowtie2db ".get_conf("mpa_dir")."/db_v20/mpa_v20_m200 --bowtie2out $metaphlan_bt ".
                    "--bt2_ps sensitive-local --min_alignment_len 51 --input_type fastq >  $metaphlan_file");
    } else {
        print " metaphlan$checkmark"; $|++;
    }

    ###########################################################################
    ## run kallisto

    if (get_option("kallisto")) {
        -d "$my_processing_dir/align" || `mkdir -vp $my_processing_dir/align`;
        system_call("time kallisto quant -i ",get_conf("kallisto_idx")," --plaintext".
            ($is_paired? "" : " --single").
            " -l 200 -s 20 -t $threads".
            " -o $my_processing_dir/align $nohuman_reads_fq");
    }




    ## run the idba assembler on the nohuman reads
    my $idba_result_dir = "$my_processing_dir/assemble-$basename";
    ## TODO
    
    ###########################################################################
    ## run DIAMOND on non-human reads
    if (get_option("blastx"))  {
    -d "$my_processing_dir/blastx" || `mkdir -vp $my_processing_dir/blastx`;
    if ($is_paired) {
        if (!-f $nohuman_reads_fa_np) {
            system_call("paste -d'\\0' ".get_p($nohuman_reads_fa,1)." ".get_p($nohuman_reads_fa,2)." > $nohuman_reads_fa_np");
        }
        if (!-f $unidentified_reads_fa_np) {
            system_call("paste -d'\\0' ".get_p($unidentified_reads_fa,1)." ".get_p($unidentified_reads_fa,2)." > $unidentified_reads_fa_np");
        }
    } else {
        if (!-f $nohuman_reads_fa_np) {
            system_call("gunzip -c $nohuman_reads_fq_filtered | fq2fa > $nohuman_reads_fa_np");
        }

    }
    my $diamond_res = "$my_processing_dir/blastx/$basename_m";
    if (!-s "$diamond_res.daa" || get_option("force")) {
        system_call("time",get_conf("diamond_bin")," blastx -d ",get_conf("diamond_nr_database")," -q $nohuman_reads_fa_np -a $diamond_res -p $threads");
        system_call("time",get_conf("diamond_bin")," view -a $diamond_res.daa -o $diamond_res.m8");
    } else {
        print " diamond$checkmark"; $|++;
    }

    ## run mtools
    my $mtools_res = "$my_processing_dir/blastx/$basename_m.lca";
    if ((!-s "$mtools_res" || get_option("force")) && -f "$diamond_res.m8") {
        #system_call("time $mtools_dir/bin/lcamapper.sh -i $diamond_res.m8 -f Detect -ms 50 -me 0.01 -tp 50 -gt $mtools_dir/data/gi_taxid_prot.bin -o $mtools_res");
        system_call("time",get_conf("mtools_dir")."/bin/lcamapper.sh -i $diamond_res.m8 -f Detect -ms 70 -me 0.001 -tp 70 -gt ",get_conf("mtools_dir")."/data/gi_taxid_prot.bin -o $mtools_res");
    } else {
        print " lca$checkmark"; $|++;
    }

    }

    ###########################################################################
    ## run CENTRIFUGE
    if (get_option("centrifuge")) {
    my $centrifuge_file = "$my_processing_dir/centrifuge/$basename_m.centrifuge.out";
    my $centrifuge_file2 = "$my_processing_dir/centrifuge/$basename_m.centrifuge.out.gz";
    my $centrifuge_report = "$my_processing_dir/centrifuge/$basename_m.centrifuge.report";
    my $centrifuge_report2 = "$my_processing_dir/centrifuge/$basename_m.centrifuge2.report";
    -d "$my_processing_dir/centrifuge" || `mkdir -vp $my_processing_dir/centrifuge`;

    if (check_file_ok($centrifuge_file2,undef,'force-centrifuge')) {
      #my $input_fastq = $new_fastq_file;
      my $input_fastq = $nohuman_reads_fq_filtered;
      my $cf_opts;
      if ($is_paired) {
          (my $input_fastq1 = $input_fastq) =~ s/%/1/g or die "$input_fastq??";
          (my $input_fastq2 = $input_fastq) =~ s/%/2/g or die "$input_fastq??";
          $cf_opts = "-1 $input_fastq1 -2 $input_fastq2";
      } else {
          $cf_opts = "-U $input_fastq";
      }

      system_call("time",get_conf("centrifuge_bin"),"--mm -x ",get_conf("centrifuge_db")," -p $threads -k 1 ".
                    " --report-file $centrifuge_report --host-taxids 9606,32630".
                    " --tab-fmt-cols readID,seqID,taxID,score,2ndBestScore,hitLength,queryLength,numMatches,readSeq".
                    " $cf_opts | tee $centrifuge_file | awk '\$3 != 9606 && \$3 != 32630' | sed 1d | sort -t \$'\t' -k3,3 -k6,6n | bgzip -c > $centrifuge_file2");
      system_call("time tabix -s 3 -b 6 -e 6 $centrifuge_file2");
    }

    if (check_file_ok($centrifuge_report2,undef,'force-centrifuge')) {
      system_call("time ~/projects/centrifuge/centrifuge-kreport -x ",get_conf("centrifuge_db")," --min-length=50 $centrifuge_file".
                    " > $centrifuge_report2");
    }
    print " centrifuge$checkmark"; $|++;
    }

    if (get_option("centrifuge2")) {
    my $centrifuge_file = "$my_processing_dir/centrifuge2/$basename_m.centrifuge.out";
    my $centrifuge_file2 = "$my_processing_dir/centrifuge2/$basename_m.centrifuge.out.gz";
    my $centrifuge_report = "$my_processing_dir/centrifuge2/$basename_m.centrifuge.report";
    -d "$my_processing_dir/centrifuge2" || `mkdir -vp $my_processing_dir/centrifuge2`;

    my $centrifuge_bin = "~/projects/centrifuge/centrifuge";
    if (check_file_ok($centrifuge_file2,undef,'force-centrifuge')) {
      #my $input_fastq = $new_fastq_file;
      my $input_fastq = $nohuman_reads_fq_filtered;
      my $cf_opts;
      if ($is_paired) {
          (my $input_fastq1 = $input_fastq) =~ s/%/1/g or die "$input_fastq??";
          (my $input_fastq2 = $input_fastq) =~ s/%/2/g or die "$input_fastq??";
          $cf_opts = "-1 $input_fastq1 -2 $input_fastq2";
      } else {
          $cf_opts = "-U $input_fastq";
      }

      system_call("time $centrifuge_bin --mm -x ",get_conf("centrifuge_db")," -p $threads -k 1 ".
                    " --report-file $centrifuge_report --host-taxids 9606,32630 --min-totallen 50".
                    " $cf_opts | bgzip -c > $centrifuge_file2");
        #system_call("time tabix -s 3 -b 6 -e 6 $centrifuge_file2");
    }

    print " centrifuge2$checkmark"; $|++;
    }

    ###########################################################################
    ## run HUMANN
    if (get_option("humann2")) {
    my $humann2_outdir = "$my_processing_dir/humann2";
    -d "$my_processing_dir/humann2" || `mkdir -vp $my_processing_dir/humann2`;

    my $humann2_bin = "humann2";
    if (check_file_ok($humann2_outdir,undef,'force-humann2')) {
      my $input_fastq = $new_fastq_file;
      my $cf_opts;
      if ($is_paired) {
          (my $input_fastq1 = $input_fastq) =~ s/%/1/g or die "$input_fastq??";
          (my $input_fastq2 = $input_fastq) =~ s/%/2/g or die "$input_fastq??";
          #$cf_opts = "-1 $input_fastq1 -2 $input_fastq2";
          die "humann2 does not work with paired files";
      } else {
          $cf_opts = "--input $nohuman_reads_fq";
      }

      system_call("time $humann2_bin $cf_opts --output $humann2_outdir");
    } else {
        print " humann2$checkmark"; $|++;
    }
    }



    print " \n";
   
}

sub get_conf {
    my ($c) = @_;
    die "$c is not specified in the configuration (PipelineConf.pm)" if (!defined $conf{$c});
    return $conf{$c};
}

sub get_p {
    my ($s,$p) = @_;
    $s =~ s/%/$p/ if defined $p;
    return $s;
}

sub filter_w_bowtie {
    return run_bt_hisat("bowtie2","--local",@_);
}

sub filter_w_hisat {
    return run_bt_hisat("/scratch0/igm3/fbreitwieser/test-microbiomics-sw/hisat2/hisat2", "", @_);
}

sub run_bt_hisat {
    my ($prog,$params,$input_fastq,$filtered_output_fastq,$sam_file,$bowtie2_idx,$is_paired, $bt2_opts) = @_;

    if ($is_paired) {
        (my $input_fastq1 = $input_fastq) =~ s/%/1/g or die "$input_fastq??";
        (my $input_fastq2 = $input_fastq) =~ s/%/2/g or die "$input_fastq??";
        $bt2_opts .= " -1 $input_fastq1 -2 $input_fastq2";
        $bt2_opts .= " --un-conc '$filtered_output_fastq'" if defined $filtered_output_fastq;
    } else {
        $bt2_opts .= " -U $input_fastq";
        $bt2_opts .= " --un '$filtered_output_fastq'" if defined $filtered_output_fastq;
    }

    if (!-s $sam_file || get_option("force")) {
        system_call("$prog $params $bam_params -x $bowtie2_idx $bt2_opts | sam2sortedbam $sam_file");
    }
    return $filtered_output_fastq;
}

sub run_sortmerna {
    my ($input_fastq,$filtered_output_fastq,$rrna_output_fastq,$is_paired) = @_;

    my $idx="/home/fbreitwieser/src/sortmerna/rRNA_databases/rfam-5.8s-database-id98.fasta,/home/fbreitwieser/src/sortmerna/index/rfam-5.8s-database-id98:/home/fbreitwieser/src/sortmerna/rRNA_databases/rfam-5s-database-id98.fasta,/home/fbreitwieser/src/sortmerna/index/rfam-5s-database-id98:/home/fbreitwieser/src/sortmerna/rRNA_databases/silva-arc-16s-id95.fasta,/home/fbreitwieser/src/sortmerna/index/silva-arc-16s-id95:/home/fbreitwieser/src/sortmerna/rRNA_databases/silva-arc-23s-id98.fasta,/home/fbreitwieser/src/sortmerna/index/silva-arc-23s-id98:/home/fbreitwieser/src/sortmerna/rRNA_databases/silva-bac-16s-id90.fasta,/home/fbreitwieser/src/sortmerna/index/silva-bac-16s-id90:/home/fbreitwieser/src/sortmerna/rRNA_databases/silva-bac-23s-id98.fasta,/home/fbreitwieser/src/sortmerna/index/silva-bac-23s-id98:/home/fbreitwieser/src/sortmerna/rRNA_databases/silva-euk-18s-id95.fasta,/home/fbreitwieser/src/sortmerna/index/silva-euk-18s-id95:/home/fbreitwieser/src/sortmerna/rRNA_databases/silva-euk-28s-id98.fasta,/home/fbreitwieser/src/sortmerna/index/silva-euk-28s-id98 --reads tumor_R2.trimmed.fq.gz.fq";

    my $opts = "-a $threads --num_alignments 1 --fastx --log --ref $idx";
    if ($is_paired) {
        (my $input_fastq1 = $input_fastq) =~ s/%/1/ or die "$input_fastq??";
        (my $filtered_output_fastq1 = $filtered_output_fastq) =~ s/%/1/ or die "$filtered_output_fastq??";
        (my $rrna_output_fastq1 = $rrna_output_fastq) =~ s/%/1/ or die "$rrna_output_fastq??";

        if (!-s $filtered_output_fastq1.".fq" || get_option("force")) {
        system_call("sortmerna $opts --reads $input_fastq1 --other $filtered_output_fastq1 --aligned $rrna_output_fastq1");
        }

        (my $input_fastq2 = $input_fastq) =~ s/%/2/ or die "$input_fastq??";
        (my $filtered_output_fastq2 = $filtered_output_fastq) =~ s/%/2/ or die "$filtered_output_fastq??";
        (my $rrna_output_fastq2 = $rrna_output_fastq) =~ s/%/2/ or die "$rrna_output_fastq??";
        if (!-s $filtered_output_fastq1.".fq" || get_option("force")) {
        system_call("sortmerna $opts --reads $input_fastq2 --other $filtered_output_fastq2 --aligned $rrna_output_fastq2");
        }

    } else {
        if (!-s $filtered_output_fastq.".fq" || get_option("force")) {
        system_call("sortmerna $opts --reads $input_fastq --other $filtered_output_fastq --aligned $rrna_output_fastq");
        }

    }


    return $filtered_output_fastq.".fq";
}

sub is_newer {
    my ($file1, $file2) = @_;
    ## -C returns the age of the file, i.e. test if file1 is younger
    return -C $file1 < -C $file2;
}

sub check_file_ok {
    my ($file, $parent_file, $force_option) = @_;

    if (get_option("verbose")) {
        print STDERR "Checking if $file exists ...\n";
    }

    my $positive_result = 0;
    if (get_option("force") || (defined $force_option && option_defined($force_option))) {
        print STDERR " --force specified, (re)creating file in any way.\n";
        $positive_result = 1;
    }

    if (!-f $file) {
        print STDERR " Creating $file... \n";
        return 1;
    }
    if (!-s $file) {
        print STDERR " $file exists, but has zero size. Recreating ...\n";
        return 1;
    }
    if (defined $parent_file && is_newer($parent_file,$file)) {
        print STDERR " $parent_file is newer than $file, recreating it ...\n";
        return 1;
    }

    return $positive_result;
}



