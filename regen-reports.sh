#!/bin/bash
#
# regen-reports.sh
# Copyright (C) 2015 fbreitwieser <fbreitwieser@igm2>
#
# Distributed under terms of the MIT license.
#

set -xeu

FQDIR=/scratch0/igm3/fbreitwieser/caapa/2-unmapped_fq
FQDIR=/scratch0/igm3/fbreitwieser/caapa/2-unmapped_fq
FQDIR=/scratch0/igm3/fbreitwieser/HIV_HCV/raw_links
DB=/scratch0/igm3/data/krakendbs/current

for KRAKEN_FILE in $*; do
    KRAKEN_REPORT=${KRAKEN_FILE//kraken/report}
    echo $KRAKEN_REPORT
    BN=`basename $KRAKEN_FILE .kraken`
    #FQ=$FQDIR/${BN}_%.fq.gz
    FQ=$FQDIR/${BN}_R%.fq.gz
    kraken-report-modif --paired --db=$DB $KRAKEN_FILE $FQ > $KRAKEN_REPORT
done
