package Utils;
use strict;
use Term::ANSIColor;
use File::Basename;
use Data::Dumper;
use Getopt::Long;
use Time::Duration;
use IO::File;
use Carp 'verbose';
$SIG{ __DIE__ } = sub { Carp::confess( @_ ) };
#use Carp::Always;

use FindBin '$Bin';

use Exporter qw(import);
our @EXPORT_OK=qw(system_call_f system_call_l system_call log_system_calls get_option option_defined);
use POSIX qw(strftime);

my $bin_dir=$Bin;
my $system_col = "white on_black";
my %options = (
    pretend => 0,
    verbose => 0
);
our $log_file;

sub timestamp {
    return strftime("%Y-%m-%d %H:%M:%S", localtime(time));
}

sub log_system_calls {
    ($log_file) = @_;
}

sub log_msg {
    return unless defined $log_file;
    open (my $LOG_FILE,">>",$log_file) or die "Cannot append to $log_file: $!";
    print $LOG_FILE "[",timestamp(),"] ",@_,"\n";
    close $LOG_FILE;
}

sub print_msg {
    my ($msg, $col) = @_;
    print STDERR "[",timestamp(),"] ", colored($msg, $col),"\n";
}

sub print_and_log_msg {
    log_msg($_[0]);
    print_msg(@_);
}

sub system_call_f { ## logged stderr, but moving stdout into a file (temp first)
    system_call1(0,1,@_);
}

sub system_call_l { ## logged stdout/stderr from system call
    system_call1(1,0,@_);
}

sub system_call {  ## system call w/o log of its stdout/stderr
    system_call1(0,0,@_);
}

$ENV{SHELL}='bash -o pipefail -euc';

sub system_call1 {
    my ($do_log,$pipe_res_file,@cmds) = @_;
    my $cmd;
    if ($pipe_res_file) {
        die unless scalar(@cmds) >= 2;
        my $res_file = pop @cmds;
        $cmd = "@cmds > $res_file.tmp && mv -v $res_file.tmp $res_file";
    } else {
        $cmd = "@cmds";
    }
    my $time_start = time();
    my $msg = "SYSTEM $cmd";
    print_msg($msg , "blue");
    if (get_option("pretend")) {

    } else {
        log_msg($msg);
        if ($do_log) {
            $cmd = "$cmd 2>&1 | $bin_dir/ts -s | tee -a '$bin_dir/$log_file'";
        }
        if (system($cmd) != 0) {
            print_and_log_msg("system $cmd failed after ".duration(time() - $time_start).": $!","red");
            die;
        }
        if ((time() - $time_start) >= 1) {
            print_and_log_msg(sprintf("finished, took %s", duration(time() - $time_start)), "green");
        }
    }
}



sub system_exec {
    print colored(" SYSTEM EXEC @_\n" , $system_col);
    if (get_option("pretend")) {

    } else {
        open P,join(" ",@_)." |" or die "error running command $!";
        my @data=<P>;
        close P;
        print colored(" finished\n",$system_col);
        #my $exit_value=$? >> 8;
        #die if $exit_value != 0;
    	return(@data);
    }
}

sub get_options {
    my ($usage, @opt_spec) = @_;
    push (@opt_spec,
         ['pretend',    "pretend - no system calls", 0],
         ['help',       "print usage message and exit", 0 ],
         ['verbose',    "be verbose",         0 ]);

    my @gopts;
    foreach my $optt (@opt_spec) {
        $usage .= "\n";
        $usage .= $optt->[0] if (scalar(@$optt) == 1);
        next unless (scalar(@$optt) >= 3);

        (my $optts = $optt->[0]) =~ s/[=!].*//;
        $options{$optts} = $optt->[2];

        if (scalar(@$optt) == 3) {
            $usage .= sprintf(
                "\t%-30s %s %s",
                colored("--".$optt->[0],"green"),
                colored($optt->[1],"reset"),
                colored("[default=".(defined $optt->[2]? $optt->[2] : '<undef>')."]","white"));
        }
        push @gopts,$optt->[0];
    }
    $usage .= "\n";

    GetOptions(\%options,@gopts) || die $usage;

    print($usage), exit if get_option("help");
    return($usage);
}

sub get_option {
    my ($option) = @_;
    die colored("$option is not a valid option","red") if (!defined $options{$option});
    return $options{$option};
}

sub option_forced {
    my ($option) = @_;
    if (!defined $option) {
        return get_option("force");
    } else {
        die "$option is not a valid option" if (!defined $options{$option});
        return get_option("force") || get_option($option);
    }
}

sub option_defined {
    my ($option) = $_[0];
    return defined $options{$option};
}



sub get_option_or_default {
    my ($option,$default) = @_;
    return (defined $options{$option}? $options{$option} : $default);
}



1;
