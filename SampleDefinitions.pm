package SampleDefinitions;

our %sample_type = (
    patients => {
        name => 'patients',
        fname_pattern=>'^(([A-Za-z]+)([0-9]+))-([A-Za-z_]+)-([RD]NA)-(S[0-9]+)-(R[0-9]+)$',
        re_resnames => [
            'code',
            'doctor',
            'patient number',
            'sample site',
            'sequencing type',
            'sample number',
            'run number'
        ],
        re_res => [
             undef,   # code
             {"AV" => "Arun Venkatesan", "CP" => "Carlos Pardo","AS"=>"AS"},
             undef,   # patient number
             {"Brain" => 1, "CSF" => 1},
             undef,   # RNA/DNA
             undef,   # sample number
             undef    # run number
              ],
        sequencing_type=>4,   ## third field in regular expression
        ext=>'.fastq.gz',
        move_fasta=>1,
        paired=>0,
        #staging_dir=>'staging'
    },
    brain_paper_patients => {
        name => 'patients',
        fname_pattern=>'^(CP_([A-Za-z]+)([0-9]+))-([A-Za-z_]+)-([RD]NA)-(S[0-9]+)-(R[0-9]+)$',
        re_resnames => [
            'code',
            'doctor',
            'patient number',
            'sample site',
            'sequencing type',
            'sample number',
            'run number'
        ],
        re_res => [
             undef,   # code
             {"AV" => "Arun Venkatesan", "CP" => "Carlos Pardo"},
             undef,   # patient number
             {"Brain" => 1, "CSF" => 1},
             undef,   # RNA/DNA
             undef,   # sample number
             undef    # run number
              ],
        sequencing_type=>4,   ## third field in regular expression
        ext=>'.fastq.gz',
        move_fasta=>1,
        paired=>0,
        #staging_dir=>'staging'
    },
    cp_patients => {
        name => 'cp_patients',
        fname_pattern=>'(CP_([A-Za-z]+)([0-9]+))-([A-Za-z_]+)-([RD]NA)-(S[0-9]+)-(R[0-9]+)',
        re_resnames => [
            'code',
            'doctor',
            'patient number',
            'sample site',
            'sequencing type',
            'sample number',
            'run number'
        ],
        re_res => [
             undef,   # code
             {"AV" => "Arun Venkatesan", "CP" => "Carlos Pardo"},
             undef,   # patient number
             {"Brain" => 1, "CSF" => 1},
             undef,   # RNA/DNA
             undef,   # sample number
             undef    # run number
              ],
        sequencing_type=>4,   ## third field in regular expression
        ext=>'.fastq.gz',
        move_fasta=>0,
        paired=>0,
        #staging_dir=>'staging'
    },


    cp_patients2 => {
        name => 'patients',
        fname_pattern=>'(CP_([A-Za-z]+)([0-9]+))-([A-Za-z_]+)-([RD]NA)-(S[0-9]+)-(R[0-9]+)_%',
        re_resnames => [
            'code',
            'doctor',
            'patient number',
            'sample site',
            'sequencing type',
            'sample number',
            'run number'
        ],
        re_res => [
             undef,   # code
             {"AV" => "Arun Venkatesan", "CP" => "Carlos Pardo"},
             undef,   # patient number
             {"Brain" => 1, "CSF" => 1},
             undef,   # RNA/DNA
             undef,   # sample number
             undef    # run number
              ],
        sequencing_type=>4,   ## third field in regular expression
        ext=>'.fastq.gz',
        move_fasta=>0,
        paired=>1,
        #staging_dir=>'staging'
    },


    pthielen_paired => {
        name => 'pthielen',
        fname_pattern=>'^(([A-Za-z]+)([0-9]+))-([A-Za-z_]+)-([RD]NA)-(S[0-9]+)-(R[0-9]+)_%$',
        re_resnames => [
            'code',
            'doctor',
            'patient number',
            'sample site',
            'sequencing type',
            'sample number',
            'run number'
        ],
        re_res => [
             undef,   # code
             {"AV" => "Arun Venkatesan", "CP" => "Carlos Pardo", "PT" => "Peter Thielen"},
             undef,   # patient number
             {"Brain" => 1, "CSF" => 1},
             undef,   # RNA/DNA
             undef,   # sample number
             undef    # run number
              ],
        sequencing_type=>4,   ## third field in regular expression
        ext=>'.fastq.gz',
        paired=>1,
        #staging_dir => 'staging/pthielen'
    },
    pthielen => {
        name => 'pthielen',
        fname_pattern=>'^(([A-Za-z]+)([0-9]+))-([A-Za-z_]+)-([RD]NA)-(S[0-9]+)-(R[0-9]+)$',
        re_resnames => [
            'code',
            'doctor',
            'patient number',
            'sample site',
            'sequencing type',
            'sample number',
            'run number'
        ],
        re_res => [
             undef,   # code
             {"AV" => "Arun Venkatesan", "CP" => "Carlos Pardo", "PT" => "Peter Thielen"},
             undef,   # patient number
             {"Brain" => 1, "CSF" => 1},
             undef,   # RNA/DNA
             undef,   # sample number
             undef    # run number
              ],
        sequencing_type=>4,   ## third field in regular expression
        ext=>'.fastq.gz',
        paired=>0,
        #staging_dir => 'staging/pthielen'
    },

    pthielen2 => {
        name => 'pthielen',
        fname_pattern=>'(.*)-PT-.*_R%_001',
        sequencing_type=>'DNA',
        ext=>'.fastq.gz',
        paired=>1,
        #staging_dir => '/scratch0/igm3/fbreitwieser/metagenomics-data/pthielen-new-files'
    },
    pthielen3 => {
        name => 'pthielen3',
        fname_pattern=>'MURI_2_.*_(S[0-9]+)_L[0-9]+_R%_001',
        sequencing_type=>'DNA',
        ext=>'.fastq.gz',
        paired=>1,
        #staging_dir => '/scratch0/igm3/fbreitwieser/metagenomics-data/2015-11-13-pthielen'
    },
    ABalagopal_Apr2016 => {
        name => 'ashwin',
        fname_pattern=>'C8L33ANXX_(.*)_%',
        sequencing_type=>'DNA',
        ext=>'.fastq.gz',
        paired=>1,
        ## /scratch0/igm3/cbc_core/Ashwin_Apr2016/FASTQ on IGM3
        #staging_dir => '/scratch0/igm3/cbc_core/Ashwin/FASTQ'
    },
    caapa => {
        name => 'caapa',
        fname_pattern=>'^(LP[0-9]{7,7}-DNA_[A-Z][0-9][0-9])_unmapped_%$',
        sequencing_type=>'DNA',
        ext=>'.fq.gz',
        paired=>1,
        #staging_dir => '/scratch0/igm3/fbreitwieser/caapa/2-unmapped_fq'
    },
    caapa2 => {
        name => 'caapa2',
        fname_pattern=>'^(LP[0-9]{7,7}-DNA_[A-Z][0-9][0-9])_%$',
        sequencing_type=>'DNA',
        ext=>'.fq.gz',
        paired=>1,
        #staging_dir => '/scratch0/igm3/fbreitwieser/caapa/2-unmapped_fq'

    },
    cindysears => {
        name => 'colorectal_cancer_microbiome',
        fname_pattern => '(.*)_R%.trimmed',
        sequencing_type => 'RNA',
        ext => '.fq.gz',
        paired => 1,
        #staging_dir => "/scratch0/igm3/salzberg/CindySears/Analysis/Data/Trimmed"
    },
    greninger_et_al => {
        name => 'greninger lancet enterovirus',
        fname_pattern => '(SRR.*)_%',
        ext =>'.fastq.gz',
        paired => 1,
        sequencing_type => 'RNA',
        #staging_dir => "/scratch0/igm3/fbreitwieser/metagenomics-data/greninger_lancet_enterovirus"
    },
    greninger_et_al_unpaired => {
        name => 'greninger lancet enterovirus',
        fname_pattern => '(SRR[^_]*)',
        ext =>'.fastq.gz',
        paired => 0,
        sequencing_type => 'RNA', 
        #staging_dir => "/scratch0/igm3/fbreitwieser/metagenomics-data/greninger_lancet_enterovirus"
    },
    ramy_hiv_hcv => {
        name => 'ramy HIV HCV',
        fname_pattern => '[0-9](.*)_R%',
        ext =>'.fastq',
        paired => 1,
        sequencing_type => 'RNA',
        #staging_dir => "/scratch0/igm3/fbreitwieser/HIV_HCV/raw_links"
    },
    tickborne_seq => {
        name => 'Tick borne disease seq',
        fname_pattern => '(.*)_R%',
        ext =>'.fastq.gz',
        paired => 1,
        sequencing_type => 'DNA',
        #staging_dir => "/scratch0/igm3/Tick_borne_seq/raw_links"
    },
    ecap => {
        name => 'Ecap',
        fname_pattern => 'af([0-9]+)\.\w*',
        ext =>'.fastq.gz',
        paired => 0,
        sequencing_type => 'DNA',
        #staging_dir => "/home/fbreitwieser/ecap"
    },
    ebola_samples => {
        name => 'greninger lancet enterovirus',
        fname_pattern => '(SRR.*)_%',
        ext =>'.fastq',
        paired => 1,
        sequencing_type => 'RNA',
        #staging_dir => "/scratch0/igm3/fbreitwieser/metagenomics-data/ebola_samples"
    },
    cornea_samples => {
        name => 'corenea samples',
        fname_pattern => 'CEbe_.*_(S[0-9]*)_merged_R%',
        ext => '.fastq.gz',
        paired => 1,
        sequencing_type => 'DNA',
        #staging_dir => "/scratch0/igm3/salzberg/Eberhart_corneal_samples"
    },
    cornea_samples2 => {
        name => 'corenea samples',
        fname_pattern => 'CEbe-.*_(S[0-9]*)_merged_R%',
        ext => '.fastq.gz',
        paired => 1,
        sequencing_type => 'DNA',
        #staging_dir => "/scratch0/igm3/salzberg/Eberhart_corneal_samples_2"
    },
    gyarmati_et_al_SR_2016 => {
        name => 'gyarmati_et_al_SR_2016',
        fname_pattern => '(.*)_%',
        ext => '.fastq.gz',
        paired => 1,
        sequencing_type => 'DNA',
        #staging_dir => "/scratch0/igm3/fbreitwieser/microbiome-pipeline/staging/sra"
    }


);


1;
